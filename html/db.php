<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        /*Google Font */
        @import url('https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');
    
        /* CSS Reset */
        *{
            text-decoration: none;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            list-style: none;
        }
        header{
            font-family: 'Poppins', sans-serif;
        }
        nav{
            background: #040b14;
            height: 80px;
            width: 100%;
            margin-bottom: 30px;
        }
        label.logo{
            color: white;
            font-size: 35px;
            line-height: 80px;
            padding: 0 100px;
            font-weight: 700;
        }
        nav ul{
            float: right;
            margin-right: 20px;
        }
        nav ul li{
            display: inline-block;
            line-height: 80px;
            margin: 0 5px;
        }
        nav ul li a{
            color: white;
            font-size: 17px;
            font-weight: bold;
            text-transform: uppercase;
        }
        a.active,a:hover{
            background: #00A78E;
            transition: .5s;
        }
    </style>
    <title>Database Page</title>
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><a href="index.html">Profile</a></li>
                <li><a href="interested.html">Interested</a></li>
                <li><a href="about_su.html">About_SU</a></li>
                <li><a href="db.php"class="active">>Database</a></li>
            </ul>
        </nav>
    </header>

    <?php
            $servername = "db";
            $username = "devops";
            $password = "devops101";

            $dbhandle = mysqli_connect($servername, $username, $password);
            $selected = mysqli_select_db($dbhandle, "titanic");

            echo "Connected database server<br>";
            echo "Selected database";
    ?>

    <script></script>
    <script src="https://kit.fontawesome.com/aa5f332820.js" crossorigin="anonymous"></script>
</body>

</html>